const Ship = require('./Ship');
const Planet = require('./Planet');
const Geometry = require('./Geometry');

const constants = global.constants;
var utils = require('../utils');

const Minilog = require('../minilog');
const log = Minilog('GameMap').debug;

class GameMap {
    constructor({ myPlayerId, width, height }) {
        this._myPlayerId = myPlayerId;
        this._width = width;
        this._height = height;

        this._playerIds = [];
        this._planets = [];
        this._ships = [];
        this._shipById = {};
        this._enemyShips = [];
        this._shipsByPlayerId = {};
    }

    get myPlayerId() {
        return this._myPlayerId;
    }

    get width() {
        return this._width;
    }

    get height() {
        return this._height;
    }

    addPlayerId(playerId) {
        this._playerIds.push(playerId);
    }

    /**
     * add ships to a specified player. only call this method explicitly to setup a game state for your unit tests.
     * engine will call this method on your behalf during a real game.
     * @param {number} playerId player id
     * @param {object[]} shipsParams ship params
     * @see strategies.test.js
     */
    addPlayerShips(playerId, shipsParams) {
        const existingShips = this._shipsByPlayerId[playerId] || [];
        const newShips = shipsParams.map(p => new Ship(this, playerId, p));

        this._shipsByPlayerId[playerId] = existingShips.concat(newShips);
        if (playerId !== this.myPlayerId) {
            this._enemyShips = this._enemyShips.concat(newShips);
        }

        this._ships = this._ships.concat(newShips);
        newShips.forEach(s => this._shipById[s.id] = s);
    }

    /**
     * add planets. only call this method explicitly to setup a game state for your unit tests.
     * engine will call this method on your behalf during a real game.
     * @param {object[]} planetParams planet params
     * @see strategies.test.js
     */
    addPlanets(planetParams) {
        this._planets = this._planets.concat(planetParams.map(p => new Planet(this, p)));
    }

    get numberOfPlayers() {
        return this._playerIds.length;
    }

    /**
     * list of all ships
     * @returns {Ship[]}
     */
    get allShips() {
        return this._ships;
    }

    /**
     * list of ships that belong to you
     * @returns {Ship[]}
     */
    get myShips() {
        return this.playerShips(this.myPlayerId);
    }

    /**
     * list of ships that belong to your enemy(ies)
     * @returns {Ship[]}
     */
    get enemyShips() {
        return this._enemyShips;
    }

    /**
     * list of ships that belong to a specified player id
     * @param playerId id of a player
     * @returns {Ship[]}
     */
    playerShips(playerId) {
        return this._shipsByPlayerId[playerId] || [];
    }

    /**
     * return ship instance by id
     * @param shipId ship id
     * @returns {Ship}
     */
    shipById(shipId) {
        return this._shipById[shipId];
    }

    /**
     * return ships instances by ids
     * @param [ids] ids of ships
     * @returns {Ship[]}
     */
    shipsByIds(ids) {
        return ids.map(id => this.shipById(id));
    }

    /**
     * list of planets
     * @returns {Planet[]}
     */
    get planets() {
        return this._planets;
    }
    get enemyPlanets() {
        return this._planets.filter(planet => planet.isOwnedByEnemy());
    }
    get myPlanets() {
        return this._planets.filter(planet => planet.isOwnedByMe());
    }

    _obstaclesBetween({ checkIntersections, obstaclesList, start, end, increaseRadiusBy = 0 }) {
        return obstaclesList.filter(o => !((o.id == start.id && o.entityType == start.entityType) || (o.id == end.id && o.entityType == end.entityType)))
            .filter(obstacle => {
                var endCollision = Geometry.intersectSegmentCircle(start, end, obstacle, start.radius + constants.CollisionFudge + increaseRadiusBy);

                if (obstacle.entityType == 'planet') {
                    return endCollision;
                }
                else if (obstacle.entityType == 'ship') {
                    if (checkIntersections) {
                        var temp = Geometry.intersectRectangles(
                            {
                                start: start,
                                end: end,
                            },
                            {
                                start: obstacle,
                                end: obstacle.nextFramePosition,
                            },
                            constants.SHIP_RADIUS,
                            constants.CollisionFudge,
                        );

                        if (temp) {
                            log(`Intersection between ship ${start.id} (${start.x} ${start.y}) (${end.x} ${end.y}) and ship ${obstacle.id} (${obstacle.x} ${obstacle.y}) (${obstacle.nextFramePosition.x} ${obstacle.nextFramePosition.y})`);
                        }
                        return endCollision || temp;
                    }
                    else {
                        return endCollision;
                    }
                }

            });
    }
}

module.exports = GameMap;
