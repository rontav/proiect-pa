const { exceptionLog } = require('./utils');
process.on('uncaughtException', function (err) {
    exceptionLog(err);

    process.exit(1);
});
const path = require('path');

const defaultConstants = require('./hlt/Constants');
var runtimeConstants = {};
if (process.argv.length == 3) {
    runtimeConstants = require(path.join(process.cwd(), process.argv[2]));
}
global.constants = Object.assign({}, defaultConstants, runtimeConstants);
// exceptionLog(global.constants);

const Game = require('./hlt/Game');
const Minilog = require('./minilog');
const log = Minilog('Game').debug;

const { strategy } = require('./strategies/');

// start a game with a bot named 'JsBot'
// and a strategy defaultStrategy defined in strategies.js
// it is defined a separate file so you can unit test it in strategies.test.js

Game.start({
    botName: 'KarnatakaKeralaMysore',
    preProcessing: map => {
        log('no data pre-processing performed. number of ships: ' + map.myShips.length);
    },
    strategy: (map, turnNumber) => {
        var time = process.hrtime();
        var temp = strategy(map, turnNumber);
        var diff = process.hrtime(time);

        var timeSpent = (diff[0] + diff[1] * 1e-9).toPrecision(2);
        if (timeSpent > 1.5) {
            process.exit();
        }
        log(`Time for turn ${turnNumber}:`, timeSpent);

        return temp;
    },
});
