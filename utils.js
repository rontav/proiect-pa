var fs = require('fs');
var util = require('util');

Math.toRad = (x) => x * Math.PI / 180;
Math.toDeg = (x) => x * 180 / Math.PI;

function position(entity) {
    var obj = {
        id: entity.id,
        x: entity.x,
        y: entity.y,
    };
    if (entity.radius > 0) {
        obj.radius = entity.radius;
    }
    return obj;
}
function printPosition(position) {
    return `entity ${position.id}(${position.x} ${position.y}${position.radius > 0 ? `; radius: ${position.radius}}` : ''})`;
}

function stringify(obj) {
    var cache = [];

    return JSON.stringify(obj, (key, value) => {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                // Circular reference found, discard key
                return;
            }
            // Store value in our collection
            cache.push(value);
        }
        return value;
    });
}

function exceptionLog(obj) {
    fs.writeFileSync(`uncaughtException-${(new Date()).getTime()}.log`, 'Caught exception: ' + util.inspect(obj, {
        depth: 3,
        colors: false,
    }));
}


function parseCoords(coords) {
    // console.log(`Coords:`, coords);
    var temp;
    try {
        temp = eval(coords);
    }
    catch (err) {
        //
    }
    if (typeof temp == 'object' &&
        typeof temp[0] == 'object') {
        coords = temp;
    }

    if (typeof coords == 'object') {
        if (typeof coords.x != 'undefined') {
            return coords;
        }
        if (typeof coords[0] == 'object') {
            return coords.map(parseCoords);
        }
        return { x: coords[0], y: coords[1] };
    }
    coords = coords.replace('(', '').replace(')', '').replace(',', '');
    coords = coords.split(' ');
    return {
        x: parseFloat(coords[0]),
        y: parseFloat(coords[1]),
    };
}

var counter = 0;
function draw(input) {
    if (!input) {
        return '';
    }
    input = parseCoords(input);
    // console.log('Input1: ', input1);

    var output = [];
    if (typeof input.x == 'undefined') {
        var dif = counter + 1;
        for (var i = 0; i < input.length; i++) {
            output.push(`R${counter + 1} = (${input[i].x}, ${input[i].y * -1})`);
            counter++;
        }
        output.push(`Polygon(${input.map((pos, index) => 'R' + (index + dif))})`);
    }
    else {
        output.push(`P${counter / 4} = (${input.x}, ${input.y * -1})`);
    }

    return output.join('\n\n');
}

function logRects(rect1, rect2) {
    return draw(rect1) + '\n' + draw(rect2);
}


module.exports = {
    stringify: stringify,
    exceptionLog,
    position,
    printPosition,
    logRects,
};