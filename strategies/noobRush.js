const utils = require('../utils');
const { constants, Geometry, GameMap } = require('../hlt');
const Minilog = require('../minilog');
const log = Minilog('Noobrush Strategy').debug;

var planetBookings = {};
var shipsBooked = {};
var aggressiveNoobRush = false;

function freeSpaces(planet) {
    return planet.dockingSpots - planet.dockedShips - planetBookings[planet.id].shipsComing.length;
}

var shipsLastFrame = 0;
var firstFrameDistance = 0;
var maximumShipsComing = 1;
/**
 * Custom strategy
 * @param {GameMap} gameMap
 * @returns {string[]} moves that needs to be taken. null values are ignored
 */
function noobRushStrategy(gameMap, turnNumber) {
    if (firstFrameDistance == 0) {
        let enemyPosition = {
            x: 0,
            y: 0,
        };
        for (let i = 0; i < gameMap.enemyShips.length; i++) {
            let ship = gameMap.enemyShips[i];
            enemyPosition = {
                x: enemyPosition.x + ship.x / gameMap.enemyShips.length,
                y: enemyPosition.y + ship.y / gameMap.enemyShips.length,
            };
        }
        let myPosition = {
            x: 0,
            y: 0,
        };
        for (let i = 0; i < gameMap.myShips.length; i++) {
            let ship = gameMap.myShips[i];
            myPosition = {
                x: myPosition.x + ship.x / gameMap.myShips.length,
                y: myPosition.y + ship.y / gameMap.myShips.length,
            };
        }

        firstFrameDistance = Geometry.distanceSquared(myPosition, enemyPosition);
        maximumShipsComing = (1 - firstFrameDistance / (constants.StartOfGameDefensiveness ** 2)) * constants.MaximumShipsComing;
        maximumShipsComing = Math.max(1, Math.min(Math.round(maximumShipsComing), constants.MaximumShipsComing));

        log(firstFrameDistance, maximumShipsComing);

        let bestPlanetForEnemy = gameMap.planets.sort((a, b) => Geometry.distanceSquared(enemyPosition, a) - Geometry.distanceSquared(enemyPosition, b));
        bestPlanetForEnemy = bestPlanetForEnemy[0];

        if (Geometry.distance(enemyPosition, bestPlanetForEnemy) / constants.MAX_SPEED + (constants.BASE_PRODUCTIVITY / gameMap.enemyShips.length) > Geometry.distance(myPosition, enemyPosition) / constants.MAX_SPEED) {
            aggressiveNoobRush = false;
        }
        else {
            aggressiveNoobRush = true;
        }
    }

    var planets = gameMap.planets;

    // Clear the booking queue if another ship spawned
    // We may need to disable this in the future
    if (gameMap.myShips.length != shipsLastFrame) {
        for (let i = 0; i < planets.length; i++) {
            let planet = planets[i];

            if (planetBookings[planet.id]) {
                planetBookings[planet.id].shipsComing = [];
                shipsBooked = {};
            }
            else {
                planetBookings[planet.id] = {
                    planet: planet,
                    shipsComing: [],
                };
            }
        }
    }
    shipsLastFrame = gameMap.myShips.length;

    log(`Started frame ${turnNumber}; ships: ${gameMap.myShips.map(ship => ship.id).join(', ')}`);

    for (let i = 0; i < gameMap.myShips.length; i++) {
        var ship = gameMap.myShips[i];

        if (shipsBooked[ship.id] || !ship.isUndocked()) {
            continue;
        }

        let planetsOfInterest = gameMap.planets.filter(planet => {
            return !planet.isOwnedByMe();
        });

        if (planetsOfInterest.length === 0) {
            continue;
        }
        planetsOfInterest = planetsOfInterest.sort((a, b) => Geometry.distanceSquared(ship, a) - Geometry.distanceSquared(ship, b));

        let replacedShip;
        let closestPlanet;

        for (let planetOfInterestIndex = 0; planetOfInterestIndex < planetsOfInterest.length; planetOfInterestIndex++) {
            let planet = planetsOfInterest[planetOfInterestIndex];

            if (freeSpaces(planet) <= 0) {
                var shipsToCompare = planetBookings[planet.id].shipsComing;
                let replaced = shipsToCompare.reduce(
                    (obj, comparedShip) => {
                        var temp = Geometry.distanceSquared(comparedShip, planet);
                        if (temp > obj.maxValue) {
                            return {
                                maxValue: temp,
                                ship: comparedShip,
                            };
                        }
                        else {
                            return obj;
                        }
                    },
                    {
                        maxValue: Infinity,
                        ship: undefined,
                    }
                );

                if (replaced.maxValue > Geometry.distanceSquared(ship, planet)) {
                    replacedShip = replaced.ship;
                    closestPlanet = planet;

                    break;
                }
            }
            else if (planetBookings[planet.id].shipsComing.length < maximumShipsComing) {
                closestPlanet = planet;

                break;
            }
        }
        if (!closestPlanet) {
            continue;
        }

        shipsBooked[ship.id] = closestPlanet;
        planetBookings[closestPlanet.id].shipsComing.push(ship);

        if (replacedShip) {
            planetBookings[closestPlanet.id].shipsComing.splice(planetBookings[closestPlanet.id].shipsComing.indexOf(replacedShip), 1);
        }
    }
    // log(`UQ`, Object.keys(planetBookings).map(key => planetBookings[key].shipsComing.length));

    const moves = gameMap.myShips
        .map(ship => {
            if (!ship.isUndocked()) {
                return;
            }

            if (aggressiveNoobRush) {
                var chosenTarget = gameMap.enemyShips.sort((a, b) => Geometry.distanceSquared(ship, a) - Geometry.distanceSquared(ship, b));
                chosenTarget = chosenTarget[0];

                let { output } = ship.navigate({
                    target: chosenTarget,
                    speed: constants.MAX_SPEED,
                });
                return output;
            }

            var chosenPlanet;
            if (!shipsBooked[ship.id]) {
                const planetsOfInterest = gameMap.planets.filter(planet => {
                    return planet.hasDockingSpot();
                });

                if (planetsOfInterest.length === 0) {
                    return null; // if all the planets are taken we return null - no move for this ship
                }

                // TODO_fast: instead of sorting, find a min value
                const sortedPlanets = planetsOfInterest.sort((a, b) => Geometry.distanceSquared(ship, a) - Geometry.distanceSquared(ship, b));
                chosenPlanet = sortedPlanets[0];
            }
            else {
                chosenPlanet = shipsBooked[ship.id];
            }

            if (ship.canDock(chosenPlanet)) {
                shipsBooked[ship.id] = undefined;
                planetBookings[chosenPlanet.id].shipsComing.splice(planetBookings[chosenPlanet.id].shipsComing.indexOf(ship), 1);
                return ship.dock(chosenPlanet);
            }
            else {
                var { output } = ship.navigate({
                    target: chosenPlanet,
                    speed: constants.MAX_SPEED,
                });
                return output;
            }
        });
    // if (turnNumber == 3) {
    //     return 'abc';
    // }

    return moves; // return moves assigned to our ships for the Halite engine to take
}


module.exports = { noobRushStrategy };