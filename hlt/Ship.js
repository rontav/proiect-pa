const Geometry = require('./Geometry');
const Entity = require('./Entity');
const GameMap = require('./GameMap');

const constants = global.constants;

const dockingStatus = require('./DockingStatus');

var utils = require('../utils');
const Minilog = require('../minilog');
const log = Minilog('    Ship').debug;

class Ship extends Entity {
    /**
     * @param {GameMap} gameMap map this ship belongs to
     * @param ownerId id of the owner
     * @param params ship information
     */
    constructor(gameMap, ownerId, params) {
        super(params);
        this._gameMap = gameMap;
        this._ownerId = ownerId;
        this.nextFramePosition = {
            x: params.x,
            y: params.y,
        };
        this.entityType = 'ship';
        this.isInArmy = false;

        // adding defaults to simplify game setup for unit testing
        this._params = {
            health: constants.BASE_SHIP_HEALTH,
            dockingStatus: dockingStatus.UNDOCKED,
            ...params,
        };

        if (this._params.dockingStatus !== dockingStatus.UNDOCKED) {
            log(`Ship   ${this._params.id}(${this._params.x} ${this._params.y}) is docked/docking...`);
        }
    }

    isDocked() {
        return this.dockingStatus === dockingStatus.DOCKED;
    }

    isDocking() {
        return this.dockingStatus === dockingStatus.DOCKING;
    }

    isUndocking() {
        return this.dockingStatus === dockingStatus.UNDOCKING;
    }

    isUndocked() {
        return this.dockingStatus === dockingStatus.UNDOCKED;
    }

    /**
     * determines if a planet can be docked
     * @param {Planet} planet
     * @return {boolean|number}
     */
    canDock(planet) {
        return (Geometry.distance(this, planet) <= constants.SHIP_RADIUS + planet.radius + constants.DOCK_RADIUS) &&
            (planet.hasDockingSpot()) &&
            (planet.isFree() || planet.ownerId === this.ownerId);
    }

    get ownerId() {
        return this._ownerId;
    }

    get dockingStatus() {
        return this._params.dockingStatus;
    }

    get radius() {
        return constants.SHIP_RADIUS;
    }

    get dockedPlanetId() {
        return this._params.dockedPlanetId;
    }

    get dockingProgress() {
        return this._params.dockingProgress;
    }

    get weaponCooldown() {
        return this._params.weaponCooldown;
    }

    /**
     * return {x, y} point that is <delta> distance before target
     * @param {{x, y}|Entity} target
     * @param {number} delta
     * @return {{x, y}} new point
     */
    pointApproaching(target, delta) {
        return Geometry.reduceEnd(this, target, delta);
    }

    dock(planet) {
        log(`Ship ${this._params.id}(${this.x} ${this.y}) docks...; distance to surface: ${Geometry.distance(this, planet) - planet.radius}`);
        return `d ${this.id} ${planet.id}`;
    }

    unDock() {
        return `u ${this.id}`;
    }

    thrust(magnitude, angle) {
        return `t ${this.id} ${magnitude | 0} ${angle | 0}`;
    }

    calculateScore() {
        if (this.score > 0) {
            return this.score;
        }
        else {
            this.score = 0;
        }

        if (this.isDocked() || this.isDocking()) {
            this.score = -100000;

            this._gameMap.myShips.forEach(myShip => {
                if (Geometry.distanceSquared(this, myShip) > constants.EnemyVulnerableRadius ** 2) {
                    return;
                }

                var multiplier = 1;
                if (this.isDocking()) {
                    multiplier *= 2;
                }
                this.score += multiplier * (constants.EnemyVulnerableRadius ** 2 - Geometry.distanceSquared(this, myShip));
            });
        }
        else {
            this._gameMap.myShips.forEach(myShip => {
                if (Geometry.distanceSquared(this, myShip) > constants.EnemyDangerRadius ** 2) {
                    return;
                }

                var multiplier = 1;
                if (myShip.isDocked() || myShip.isDocking()) {
                    multiplier *= 2;
                }
                else {
                    multiplier = -1;
                }
                this.score += multiplier * (constants.EnemyDangerRadius ** 2 - Geometry.distanceSquared(this, myShip));
            });
        }

        return this.score;
    }

    reachableTarget(originalTarget, speed) {
        var distance = this.distanceBetween(originalTarget);
        var angle = Geometry.angleInRad(this, originalTarget);

        var newSpeed = distance >= speed ? speed : distance;
        var target = {
            id: originalTarget.id,
            x: this.x + Math.cos(angle) * newSpeed,
            y: this.y + Math.sin(angle) * newSpeed,
        };
        return target;
    }

    findObstacles(ignoreShips, ignorePlanets, avoidFight, target) {
        var obstacles = [];
        if (!ignorePlanets) {
            obstacles.push(this._gameMap._obstaclesBetween({
                obstaclesList: this._gameMap.planets,
                start: this,
                end: target,
            }));
        }
        if (!ignoreShips) {
            obstacles.push(
                this._gameMap._obstaclesBetween({
                    checkIntersections: true,
                    obstaclesList: this._gameMap.allShips.filter(obstacleShip => Geometry.distanceSquared(this, obstacleShip) <= ((constants.MAX_SPEED + constants.SHIP_RADIUS) * 2 + constants.CollisionFudge) ** 2),
                    start: this,
                    end: target,
                    increaseRadiusBy: avoidFight ? constants.WEAPON_RADIUS + constants.MAX_SPEED : 0,
                })
            );
        }
        if (!ignoreShips && !ignorePlanets) {
            obstacles = obstacles[0].concat(obstacles[1]);
        }
        return obstacles;
    }

    /**
     * Move a ship to a specific target position (Entity). It is recommended to place the position
     * itself here, else navigate will crash into the target. If avoidObstacles is set to true (default)
     * will avoid obstacles on the way, with up to maxCorrections corrections. Note that each correction accounts
     * for angularStep degrees difference, meaning that the algorithm will naively try max_correction degrees before giving
     * up (and returning null). The navigation will only consist of up to one command; call this method again
     * in the next turn to continue navigating to the position.
     * @param {Entity} target the entity to which you will navigate
     * @param {number} keepDistanceToTarget distance to maintain to the target
     * @param {number} speed the (max) speed to navigate. if the obstacle is nearer, will adjust accordingly.
     * @param {boolean} avoidObstacles whether to avoid the obstacles in the way (simple pathfinding).
     * @param {number} maxCorrections the maximum number of degrees to deviate per turn while trying to pathfind. if exceeded returns null.
     * @param {number} angularStep the degree difference to deviate if the original destination has obstacles
     * @param {boolean} ignoreShips whether to ignore ships in calculations (this will make your movement faster, but more precarious)
     * @param {boolean} ignorePlanets whether to ignore planets in calculations (useful if you want to crash onto planets)
     */
    navigate({
        target,
        speed,
        angularStep = Math.toRad(1),
        avoidObstacles = true,
        ignoreShips = false,
        ignorePlanets = false,
        avoidFight = false,
    }) {

        if (avoidObstacles) {
            var originalTarget = target;
            var obstacles = this.findObstacles(ignoreShips, ignorePlanets, avoidFight, this.reachableTarget(target, speed));

            log(`Ship ${this._params.id}(${this.x} ${this.y}) wants to go to ${target.x} ${target.y} reachable: ${this.reachableTarget(target, speed).x} ${this.reachableTarget(target, speed).y}`);
            var maxCorrections = 180;
            var left = true;
            var tempTarget = target;

            // log(`OO: Obstacles for ship ${this.id} and target ${target.id}:`, obstacles.map(o => {
            //     return {
            //         id: o.id,
            //         type: o.entityType,
            //         x: o.x,
            //         y: o.y,
            //         radius: o.radius,
            //     };
            // }));

            while (maxCorrections && obstacles.length) {
                var tempDistance = Geometry.distance(this, tempTarget);
                var currentAngle = Geometry.angleInRad(this, tempTarget);

                var newAngle;
                if (left) {
                    newAngle = currentAngle + angularStep * (180 - maxCorrections);
                    left = false;
                }
                else {
                    newAngle = currentAngle - angularStep * (180 - maxCorrections);
                    left = true;
                }

                tempTarget = {
                    id: target.id,
                    x: target.x + Math.cos(newAngle) * tempDistance,
                    y: target.y + Math.sin(newAngle) * tempDistance,
                };

                obstacles = this.findObstacles(ignoreShips, ignorePlanets, avoidFight, this.reachableTarget(tempTarget, speed));
                maxCorrections--;

                // log(`Obstacles for ship ${this.id}:`, obstacles.map(o => {
                //     return {
                //         id: o.id,
                //         type: o.entityType,
                //         x: o.x,
                //         y: o.y,
                //         radius: o.radius,
                //     };
                // }));
                // log(`Correction for ship ${this.id}; new target is ${this.reachableTarget(tempTarget, speed).x}, ${this.reachableTarget(tempTarget, speed).y}; maxCorrection: ${maxCorrections}; currentDistance: ${tempDistance}`);
            }
            if (!maxCorrections) {
                log(`Ship ${this.id} stalled out`);
                return {
                    output: null,
                };
            }
            target = tempTarget;
        }

        target = this.reachableTarget(target, speed);
        if (originalTarget.entityType == 'planet' && Geometry.intersectSegmentCircle(this, target, originalTarget, constants.CollisionFudge + this.radius)) {
            var to = target;
            target = Geometry.intersectionPointSegmentCircle(this, target, originalTarget, constants.CollisionFudge + this.radius);
            log('Too close to planet, decrease speed', to, target);
        }
        log(`Ship ${this.id} wants to go to (${target.x} ${target.y}) to dock. Planet: (${originalTarget.x} ${originalTarget.y}) ${originalTarget.radius}; distance to surface: ${Geometry.distance(target, originalTarget) - originalTarget.radius}`);
        var distance = this.distanceBetween(target);
        var angle = Geometry.angleInRad(this, target);

        var newSpeed = distance >= speed ? speed : distance;

        this.nextFramePosition = {
            entityType: this.entityType,
            x: this.x + Math.cos(angle) * newSpeed,
            y: this.y + Math.sin(angle) * newSpeed,
        };
        log(`Ship ${this._params.id}(${this.x} ${this.y}) actual target: (${this.x + Math.cos(angle) * newSpeed} ${this.y + Math.sin(angle) * newSpeed})`);

        return {
            output: this.thrust(newSpeed, Math.toDeg(angle)),
        };
    }

    toString() {
        return 'ship. owner id: ' + this.ownerId + ': ' + JSON.stringify(this._params);
    }
}

module.exports = Ship;