const utils = require('../utils');
const { constants, Geometry, GameMap } = require('../hlt');
const Minilog = require('../minilog');
const log = Minilog('Midgame Strategy').debug;

var planetBookings = {};
var shipsBooked = {};

function freeSpaces(planet) {
    return planet.dockingSpots - planet.dockedShips.length - planetBookings[planet.id].shipsComing.length;
}
var armyShipIDs = [];
var shipIDsLastFrame = [];
var shipsLastFrame = 0;
var droneCounter = 0;

function reasign(ship) {
    var targetPlanet = shipsBooked[ship.id];
    if (targetPlanet) {
        planetBookings[targetPlanet.id].shipsComing.splice(planetBookings[targetPlanet.id].shipsComing.indexOf(ship), 1);
        delete shipsBooked[ship.id];
    }
    else if (armyShipIDs.includes(ship.id)) {
        armyShipIDs.splice(armyShipIDs.indexOf(ship.id), 1);
        ship.isInArmy = false;
    }
}
/**
 * Custom strategy
 * @param {GameMap} gameMap
 * @returns {string[]} moves that needs to be taken. null values are ignored
 */
function midgameStrategy(gameMap, turnNumber) {
    var droneToArmyPercent = constants.DroneToArmyPercent;
    var planets = gameMap.planets;

    // Clear the booking queue if another ship spawned
    // We may need to disable this in the future
    if (gameMap.myShips.length != shipsLastFrame) {
        for (let i = 0; i < planets.length; i++) {
            let planet = planets[i];

            planetBookings[planet.id] = {
                planet: planet,
                shipsComing: [],
            };
        }
        shipsBooked = {};
    }
    shipsLastFrame = gameMap.myShips.length;

    log(`Started frame ${turnNumber}; ships: ${gameMap.myShips.map(ship => ship.id).join(', ')}`);

    for (let i = 0; i < gameMap.myShips.length; i++) {
        var ship = gameMap.myShips[i];

        if (!shipIDsLastFrame.includes(ship.id)) {
            let closeEnemies = gameMap.enemyShips.filter(enemyShip => ship.distanceBetween(enemyShip) < constants.SafeRadius);

            if (closeEnemies.length > 0 && droneCounter > droneToArmyPercent) {
                reasign(ship);

                armyShipIDs.push(ship.id);
                droneCounter = 0;
            }
            else {
                droneCounter++;
                shipIDsLastFrame.push(ship.id);
            }
        }

        ship.isInArmy = armyShipIDs.includes(ship.id);

        if (!ship.isInArmy) {
            if (shipsBooked[ship.id] || !ship.isUndocked()) {
                continue;
            }

            let planetsOfInterest = gameMap.planets.filter(planet => {
                return planet.hasDockingSpot() && !planet.isOwnedByEnemy();
            });

            if (planetsOfInterest.length === 0) {
                continue;
            }
            planetsOfInterest = planetsOfInterest.sort((a, b) => Geometry.distanceSquared(ship, a) - Geometry.distanceSquared(ship, b));

            let replacedShip;
            let closestPlanet;

            for (let planetOfInterestIndex = 0; planetOfInterestIndex < planetsOfInterest.length; planetOfInterestIndex++) {
                let planet = planetsOfInterest[planetOfInterestIndex];

                if (freeSpaces(planet) <= 0) {
                    var shipsToCompare = planetBookings[planet.id].shipsComing;
                    let replaced = shipsToCompare.reduce(
                        (obj, comparedShip) => {
                            var temp = Geometry.distanceSquared(comparedShip, planet);
                            if (temp > obj.maxValue) {
                                return {
                                    maxValue: temp,
                                    ship: comparedShip,
                                };
                            }
                            else {
                                return obj;
                            }
                        },
                        {
                            maxValue: -Infinity,
                            ship: undefined,
                        }
                    );

                    if (replaced.maxValue > Geometry.distanceSquared(ship, planet)) {
                        replacedShip = replaced.ship;
                        closestPlanet = planet;

                        break;
                    }
                }
                else if (planetBookings[planet.id].shipsComing.length < constants.MaximumShipsComing) {
                    closestPlanet = planet;

                    break;
                }
            }
            if (!closestPlanet) {
                continue;
            }

            shipsBooked[ship.id] = closestPlanet;
            planetBookings[closestPlanet.id].shipsComing.push(ship);

            if (replacedShip) {
                reasign(replacedShip);
            }
        }
    }
    // log(`UQ`, Object.keys(planetBookings).map(key => planetBookings[key].shipsComing.length));

    gameMap.myPlanets.forEach(planet => {
        planet.enemiesClose = gameMap.enemyShips.filter(enemyShip => enemyShip.isUndocked() && Geometry.distanceSquared(planet, enemyShip) < (planet.radius + constants.EnemyNearbyPlanetRadius) ** 2);
        planet.alliesClose = gameMap.myShips.filter(myShip => myShip.isUndocked() && myShip.isInArmy && Geometry.distanceSquared(planet, myShip) < (planet.radius + constants.AllyNearbyPlanetRadius) ** 2);
    });

    const moves = gameMap.myShips
        .map(ship => {
            if (!ship.isUndocked()) {
                return;
            }

            if (!ship.isInArmy) {
                let armyShipsNearMe = gameMap.myShips.filter(armyShip => ship.isInArmy && Geometry.distanceSquared(ship, armyShip) < constants.ArmyNearbyRadius ** 2);
                let enemyShipsNearMe = gameMap.enemyShips.filter(enemyShip => Geometry.distanceSquared(ship, enemyShip) < (constants.EnemyNearbyRadius * (enemyShip.isDocked() || enemyShip.isDocking() ? constants.Aggressive : 1)) ** 2);

                if (armyShipsNearMe.length < enemyShipsNearMe.length) {
                    reasign(ship);

                    armyShipIDs.push(ship.id);
                    ship.isInArmy = true;
                }
            }

            var chosenTarget;
            if (!ship.isInArmy) {
                if (!shipsBooked[ship.id]) {
                    reasign(ship);
                    ship.isInArmy = true;
                }
                else {
                    chosenTarget = shipsBooked[ship.id];
                }
            }

            if (ship.isInArmy) {
                if (gameMap._enemyShips.length == 0) {
                    return;
                }

                const entitiesOfInterest = gameMap._enemyShips.concat(gameMap.myPlanets)
                    .filter(entity => (entity.entityType == 'ship' && Geometry.distanceSquared(ship, entity) <= constants.EnemyMaxDistanceForScoreCalculation ** 2) ||
                        (entity => (entity.entityType == 'planet' && Geometry.distanceSquared(ship, entity) <= (entity.radius + constants.PlanetMaxDistanceForScoreCalculation) ** 2)))
                    .sort((a, b) => {
                        a.diff = (a.calculateScore() - (Geometry.distanceSquared(ship, a) - a.radius ** 2));
                        b.diff = (b.calculateScore() - (Geometry.distanceSquared(ship, b) - b.radius ** 2));
                        var scoreDiff = b.diff - a.diff;

                        return scoreDiff;
                    });

                chosenTarget = entitiesOfInterest[0];
            }

            if (chosenTarget.entityType == 'planet') {
                if (ship.isInArmy) {
                    var targetEnemies = chosenTarget.enemiesClose.filter(enemy => Geometry.distanceSquared(enemy, chosenTarget) < (chosenTarget.radius + constants.StandGroundMaxDistance) ** 2).sort((a, b) => Geometry.distanceSquared(ship, a) < Geometry.distanceSquared(ship, b));
                    targetEnemies = targetEnemies[0];

                    let { output } = ship.navigate({
                        target: targetEnemies || chosenTarget,
                        speed: constants.MAX_SPEED,
                    });
                    return output;
                }
                else if (ship.canDock(chosenTarget)) {
                    reasign(ship);
                    return ship.dock(chosenTarget);
                }
                else {
                    let { output } = ship.navigate({
                        target: chosenTarget,
                        speed: constants.MAX_SPEED,
                    });
                    return output;
                }
            }
            else if (chosenTarget.entityType == 'ship') {
                if (ship.distanceBetween(chosenTarget) > constants.WEAPON_RADIUS) {
                    let { output } = ship.navigate({
                        target: chosenTarget,
                        speed: constants.MAX_SPEED,
                    });

                    return output;
                }
                else {
                    return;
                }
            }
        });
    // if (turnNumber == 3) {
    //     return 'abc';
    // }

    return moves; // return moves assigned to our ships for the Halite engine to take
}


module.exports = { midgameStrategy };