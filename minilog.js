const fs = require('fs');
const Minilog = require('@rontav/minilog');

Minilog
    .pipe(new Minilog.Filter()
        .deny('Halite-Game', 'debug')
        // .deny('Ship', 'debug')
        .deny('    Ship', 'debug')
        .deny('Game', 'debug')
        .deny('GameMap', 'debug')
        // .deny('Noobrush Strategy', 'debug')
        // .deny('Midgame Strategy', 'debug')
        .deny('Strategy index', 'debug')
    )
    .pipe(new Minilog.Stringifier())
    .pipe(new fs.createWriteStream('./karnatakaBot.log'));

module.exports = (e) => {
    return Minilog(e);
};