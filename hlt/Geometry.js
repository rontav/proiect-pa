var RectanglesIntersect = require('rectangles-intersect');
var utils = require('../utils');

const Minilog = require('../minilog');
const log = Minilog('Geometry').debug;

class Geometry {
    /**
     * distance squared between two points (faster than distance)
     * @param start object with {x, y} properties
     * @param end object with {x, y} properties
     * @returns {number} distance squared
     */
    static distanceSquared(start, end) {
        const dx = end.x - start.x;
        const dy = end.y - start.y;

        return dx * dx + dy * dy;
    }
    /**
     * distance between two points
     * @param start object with {x, y} properties
     * @param end object with {x, y} properties
     * @returns {number} distance
     */
    static distance(start, end) {
        const dx = end.x - start.x;
        const dy = end.y - start.y;

        return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
    }

    /**
     * angle in rad between two points
     * @param {object} start object with {x, y} properties
     * @param {object} end object with {x, y} properties
     * @returns {number} radian between 0 and 2*PI
     */
    static angleInRad(start, end) {
        const dx = end.x - start.x;
        const dy = end.y - start.y;

        const atan = Math.atan2(dy, dx);
        return atan >= 0 ? atan : (atan + 2 * Math.PI);
    }

    /**
     * find the closest point to the given end position near the given target, outside its given radius,
     * with an added fudge of min_distance.
     * @param start {object} start object with {x, y} properties
     * @param end {object} end object with {x, y} properties
     * @param delta distance by what reduce the end position
     */
    static reduceEnd(start, end, delta) {
        const angleRad = Geometry.angleInRad(start, end);

        const dx = Math.cos(angleRad) * delta;
        const dy = Math.sin(angleRad) * delta;

        return { x: end.x - dx, y: end.y - dy };
    }


    static createRectangle(start, end, height, fudge) {
        var dx = start.x - end.x;
        var dy = start.y - end.y;
        var lineLength = Math.sqrt(dx ** 2 + dy ** 2);

        if (lineLength == 0) {
            var squareDimension = height + fudge;
            return [
                [start.x + squareDimension, start.y + squareDimension],
                [start.x + squareDimension, start.y - squareDimension],
                [start.x - squareDimension, start.y - squareDimension],
                [start.x - squareDimension, start.y + squareDimension],
            ];
        }

        dx /= lineLength;
        dy /= lineLength;

        var px = (height + fudge) * (-dy);
        var py = (height + fudge) * (dx);

        return [
            [start.x + px, start.y + py],
            [end.x + px, end.y + py],
            [start.x - px, start.y - py],
            [end.x - px, end.y - py],
        ];
    }

    /**
     * Test whether two rectangles intersect.
     * @param {object} rect1 object with {start, end} properties
     * @param {object} rect2 object with {start, end} properties
     * @param {object} height rectangle height
     * @param {number} [fudge] fudge factor: additional distance to leave between the segment and circle
     * @returns {boolean} true if intersects, false - otherwise
     */
    static intersectRectangles({ start: start1, end: end1 }, { start: start2, end: end2 }, height, fudge = 0) {
        return RectanglesIntersect(
            Geometry.createRectangle(start1, end1, height, fudge),
            Geometry.createRectangle(start2, end2, height, fudge)
        );
    }

    static intersectionPointSegmentCircle(start, end, circle, fudge) {
        var a = ((start.y - circle.y) - (end.y - circle.y)) / ((start.x - circle.x) - (end.x - circle.x));
        var b = ((start.y - circle.y) - a * (start.x - circle.x));

        var r = circle.radius + fudge;
        var nuConteza = Math.sqrt(a ** 2 * r ** 2 - b ** 2 + r ** 2);
        var sol1 = {
            x: (-1 * nuConteza - a * b) / (a ** 2 + 1) + circle.x,
            y: (b - a * nuConteza) / (a ** 2 + 1) + circle.y,
        };
        var sol2 = {
            x: (nuConteza - a * b) / (a ** 2 + 1) + circle.x,
            y: (b + a * nuConteza) / (a ** 2 + 1) + circle.y,
        };
        if (Geometry.distanceSquared(start, sol1) < Geometry.distanceSquared(start, sol2)) {
            return sol1;
        }
        else {
            return sol2;
        }
    }
    /**
     * Test whether a line segment and circle intersect.
     * @param {object} start object with {x, y} properties
     * @param {object} end object with {x, y} properties
     * @param {object} circle object with {x, y, radius} properties
     * @param {number} [fudge] fudge factor: additional distance to leave between the segment and circle
     * @returns {boolean} true if intersects, false - otherwise
     */
    static intersectSegmentCircle(start, end, circle, fudge) {
        const dx = end.x - start.x;
        const dy = end.y - start.y;

        const a = dx ** 2 + dy ** 2;

        if (a === 0.0) {
            return Geometry.distanceSquared(start, circle) <= (circle.radius + fudge) ** 2;
        }

        const b = -2 * (start.x ** 2 - start.x * end.x - start.x * circle.x + end.x * circle.x +
            start.y ** 2 - start.y * end.y - start.y * circle.y + end.y * circle.y);

        const t = Math.min(-b / (2 * a), 1.0);
        if (t < 0) {
            return false;
        }

        const closestX = start.x + dx * t;
        const closestY = start.y + dy * t;
        const closestDistance = Geometry.distanceSquared({ x: closestX, y: closestY }, circle);

        return closestDistance <= (circle.radius + fudge) ** 2;
    }
}

module.exports = Geometry;