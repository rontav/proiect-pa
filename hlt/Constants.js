module.exports = {
    // Max number of units of distance a ship can travel in a turn
    MAX_SPEED: 7,
    // Radius of a ship
    SHIP_RADIUS: 0.5001,
    // Starting health of ship, also its max
    MAX_SHIP_HEALTH: 255,
    // Starting health of ship, also its max
    BASE_SHIP_HEALTH: 255,
    // Weapon cooldown period
    WEAPON_COOLDOWN: 1,
    // Weapon damage radius
    WEAPON_RADIUS: 5.0001,
    // Weapon damage
    WEAPON_DAMAGE: 64,
    // Radius in which explosions affect other entities
    EXPLOSION_RADIUS: 10.0,
    // Distance from the edge of the planet at which ships can try to dock
    DOCK_RADIUS: 4.0,
    // Number of turns it takes to dock a ship
    DOCK_TURNS: 5,
    // Number of production units per turn contributed by each docked ship
    BASE_PRODUCTIVITY: 6,
    // Distance from the planets edge at which new ships are created
    SPAWN_RADIUS: 2.0,


    CollisionFudge: 0.2,

    StartOfGameDefensiveness: 170,
    MaximumShipsComing: 5,
    DroneToArmyPercent: 99,
    ArmyNearbyRadius: 15,
    EnemyNearbyRadius: 15,

    EnemyNearbyPlanetRadius: 0,
    AllyNearbyPlanetRadius: 60,
    PlanetDefenseRatio: 100,
    StandGroundMaxDistance: 10,

    SafeRadius: 85,
    EnemyDangerRadius: 25,
    EnemyVulnerableRadius: 50,

    EnemyMaxDistanceForScoreCalculation: 100,
    PlanetMaxDistanceForScoreCalculation: 100,

    Aggressive: 1.5,

    HarrasThreshold: 10,
    MaxHarrasShips: 0,
    HarrasDistance: 10,
    HarrasRandom: 10,
};
