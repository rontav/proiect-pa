const path = require('path');

const threads = 16;
const configFileName = 'bruteForceConfig';

const spawn = require('child_process').spawn;
const fs = require('fs-extra');

var childProcesses = [];

for (let i = 0; i < threads; i++) {
    childProcesses[i] = {
        id: i,
        configFile: `${configFileName}-${i}.json`,
        process: undefined,
    };
}

var configLocked = false;
var configsToChange = {
    'StartOfGameDefensiveness': {
        value: 0,
        min: 0,
        max: 170,
        step: 1,
    },
    'MaximumShipsComing': {
        value: 1,
        min: 1,
        max: 5,
        step: 1,
    },
    'EnemyNearbyRadius': {
        value: 10,
        min: 10,
        max: 50,
        step: 1,
    },
};

var output = {};

var finishedAllConfigs = false;

function startChild(child) {
    if (finishedAllConfigs) {
        return;
    }
    if (configLocked) {
        return setTimeout(() => startChild(child));
    }

    configLocked = true;
    let running = true;
    let i = 0;
    let keys = Object.keys(configsToChange);

    while (running) {
        let configKey = keys[i];
        if (i >= keys.length) {
            break;
        }

        let config = configsToChange[configKey];
        config.value += config.step;

        if (config.value > config.max) {
            config.value = config.min;
            i++;
        }
        else {
            running = false;
        }
    }
    let nextConfigAvailable = !running;

    var tempConfig = Object.assign({}, configsToChange);
    Object.keys(tempConfig).forEach(key => {
        tempConfig[key] = tempConfig[key].value;
    });

    const command = `python`;
    let commandArgs = [
        'run.py',
        '--cmd',
        `node ../myBot.js ${child.configFile}`,
        '--round',
        '3',
    ];
    let configValues = keys.map(key => key + ' ' + configsToChange[key].value.toString().padStart(3)).join(', ');
    // console.log(child.id + ': ' + keys.map(key => key + ' ' + configsToChange[key].value).join(' '));
    // console.log(`Start ${child.id}`);

    configLocked = false;
    if (nextConfigAvailable) {
        let buffer = '';
        let err = '';

        fs.writeFileSync(path.join(process.cwd(), '/halite-2-resources/', child.configFile), JSON.stringify(tempConfig) + '\n');

        child.process = spawn(command, commandArgs, {
            cwd: path.join(process.cwd(), 'halite-2-resources'),
        });

        child.process.stdout.on('data', (data) => {
            buffer += data.toString();
            // process.stdout.write('.');
            // process.stdout.write(data.toString());
        });
        child.process.stderr.on('data', (data) => {
            err += data.toString();
            // process.stdout.write('.');
            // process.stdout.write(data.toString());
        });
        child.process.on('exit', () => {
            // process.stdout.write('\n');
            if (err) {
                console.log(buffer, err);
            }
            let roundsRegex = new RegExp(/placed (.+?) in ([0-9]+?) frames/igm);
            let rounds = [];
            let temp;
            do {
                temp = roundsRegex.exec(buffer);
                if (temp) {
                    rounds.push(`${temp[1].replace(/[a-z]/igm, '')} in ${temp[2]}`);
                }
            } while (temp);

            let regex = new RegExp(/Final score: (.+?\/.+?)\n/gim);
            buffer = buffer.substr(buffer.indexOf('Final score') - 1);
            let match = regex.exec(buffer);
            if (match && match.length) {
                output[configValues] = `${match[1]}:: ${rounds.join(', ')}`;
                // console.log(buffer);
            }
            else {
                output[configValues] = 'error';
            }
            console.log(`'${configValues}': '${output[configValues]}'`);
            // console.log(output);
            startChild(child);
        });
    }
    else {
        finishedAllConfigs = true;
        console.log(output);
    }
}

childProcesses.forEach(async child => {
    var temp = Object.assign({}, configsToChange);
    startChild(child, temp);
});