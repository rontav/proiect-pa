var { noobRushStrategy } = require('./noobRush');
var { midgameStrategy } = require('./midgame');
const Minilog = require('../minilog');
const log = Minilog('Strategy index').debug;

var noobRushEnded = false;

module.exports = {
    strategy: (gameMap, turnNumber) => {
        if (!noobRushEnded && gameMap.myShips.length <= 3) {
            return noobRushStrategy(gameMap, turnNumber);
        }
        else {
            noobRushEnded = true;

            var temp = midgameStrategy(gameMap, turnNumber);

            return temp;
        }
    },
};