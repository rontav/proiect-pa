#!/bin/bash

. ~/.nvm/nvm.sh &>/dev/null
. ~/.profile &>/dev/null
. ~/.bashrc &>/dev/null

if ! type "nvm" &>/dev/null; then
    if ! type "curl" &>/dev/null; then
        wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
    else
        curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
    fi

    if [[ $(command -v nvm) != "nvm" ]]; then
        . ~/.nvm/nvm.sh &>/dev/null
    fi
fi

nvm install 9.8.0
nvm use 9.8.0

if [[ $(nvm exec npm -v) != '5.7.1' ]]; then
    nvm exec npm i -g npm@5.7.1
fi

nvm exec npm ci

echo -e '\033[0;31mTo be able to run our bot you must run "source ~/.nvm/nvm.sh" or re-open your terminal.\033[0m'